// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'LocalStorageModule'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/dashboard/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  // leads tab and leads subviews
  .state('tab.leads', {
      url: '/leads',
      views: {
        'tab-leads': {
          templateUrl: 'templates/leads/tab-leads.html',
          controller: 'LeadsCtrl'
        }
      }
    })

    .state('tab.leads.email', {
      url: '/leads/:leadId/email',
      views: {
        'tab-leads': {
          templateUrl: 'templates/leads/leads-email.html',
          controller: 'LeadsEmailCtrl'
        }
      }
    })

    .state('tab.leads-detail', {
      url: '/leads/:leadId',
      views: {
        'tab-leads': {
          templateUrl: 'templates/leads/leads-detail.html',
          controller: 'LeadsDetailCtrl'
        }
      }
    })



    // lists and lists subviews
    .state('tab.lists', {
      url: '/lists',
      views: {
        'tab-lists': {
          templateUrl: 'templates/lists/tab-lists.html',
          controller: 'ListsCtrl'
        }
      }
    })

  .state('tab.login',{
    url:'/login',
    views: {
      'tab-login':{
        templateUrl: 'templates/tab-login.html',
        controller: 'LoginCtrl'
      }
    }
  })


  // Settings and settings subviews
  .state('tab.settings', {
    url: '/settings',
    views: {
      'tab-settings': {
        templateUrl: 'templates/settings/tab-settings.html',
        controller: 'SettingsCtrl'
      }
    }
  })



  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/login');

});
