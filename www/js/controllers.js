angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('LeadsCtrl', function($scope, Leads) {
  $scope.leads = Leads.all();
  $scope.remove = function(lead) {
    Leads.remove(lead);
  };
})

.controller('ListsCtrl', function($scope, Leads) {
  $scope.leads = Leads.all();
  $scope.remove = function(lead) {
    Leads.remove(lead);
  };
})

.controller('LoginCtrl', function($scope) {})


.controller('LeadsDetailCtrl', function($scope, $stateParams, Leads) {
  $scope.lead = Leads.get($stateParams.leadId);
})

.controller('LeadsEmailCtrl', function($scope, $stateParams, Leads) {
  $scope.lead = Leads.get($stateParams.leadId);
})

.controller('SettingsCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
